package dnsrecords

import (
	// stdlib
	"sync"
	"time"

	// local
	"go.dev.pztrn.name/ihava/internal/configuration"
	"go.dev.pztrn.name/ihava/internal/logger"

	// other
	"github.com/miekg/dns"
	"github.com/rs/zerolog"
)

var (
	log zerolog.Logger

	// Own DNS records cache. Key is an interface name.
	// ToDo: maybe not interface{} but something else?
	ownRecords      map[string][]dns.RR
	ownRecordsMutex sync.Mutex
)

// Initialize initializes package.
func Initialize() {
	log = logger.Logger.With().Str("package", "dnsrecords").Logger()

	log.Info().Msg("Initializing subsystem...")

	ownRecords = make(map[string][]dns.RR)
}

// Starts timer that checks local interfaces for addresses changes
// and updates DNS records cache accordingly. Launches in separate
// goroutine.
// This function will be interrupted immediately on shut down as
// it is just a "timed helper".
func addressChecker() {
	timer := time.NewTicker(configuration.Cfg.Station.Timeouts.InterfaceAndAddressesCheck)
	locallog := log.With().Str("subsystem", "addresses checker").Logger()
	locallog.Info().Msg("Starting asynchronous DNS records cacher...")

	updateLocalAddressesCache(locallog)

	for range timer.C {
		updateLocalAddressesCache(locallog)
	}
}

// Returns a list of records for requested interface.
func GetRecordsForInterfaceName(interfaceName string, requestID uint16) *dns.Msg {
	response := &dns.Msg{}
	var records []dns.RR

	ownRecordsMutex.Lock()
	generatedRecords, found := ownRecords[interfaceName]
	ownRecordsMutex.Unlock()

	if found {
		records = generatedRecords
	}

	response.MsgHdr = dns.MsgHdr{
		Id:            requestID,
		Response:      true,
		Opcode:        dns.OpcodeQuery,
		Authoritative: true,
	}
	response.Compress = true
	response.Answer = records

	return response
}

// Start starts subsystem things.,
func Start() {
	go addressChecker()
}
