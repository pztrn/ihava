package vars

import (
	// stdlib
	"net"
)

var (
	// Private addresses networks as from RFC1918. Used for addresses
	// validation.
	PrivateNets []*net.IPNet
	// Processed on Server.Initialize()
	privateV4Nets = []string{
		"10.0.0.0/8",
		"172.16.0.0/12",
		"192.168.0.0/16",
	}
	privateV6Nets = []string{
		"fd00::/8",
	}
)

func CompilePrivateNetsDefinitions() {
	PrivateNets = make([]*net.IPNet, 0, len(privateV4Nets)+len(privateV6Nets))
	for _, v4net := range privateV4Nets {
		_, parsedCIDR, _ := net.ParseCIDR(v4net)
		PrivateNets = append(PrivateNets, parsedCIDR)
	}

	for _, v6net := range privateV6Nets {
		_, parsedCIDR, _ := net.ParseCIDR(v6net)
		PrivateNets = append(PrivateNets, parsedCIDR)
	}
}
