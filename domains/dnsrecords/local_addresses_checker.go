package dnsrecords

import (
	// stdlib
	"net"
	"strings"

	// local
	"go.dev.pztrn.name/ihava/internal/configuration"
	"go.dev.pztrn.name/ihava/internal/logger"
	"go.dev.pztrn.name/ihava/internal/vars"

	// other
	"github.com/miekg/dns"
	"github.com/rs/zerolog"
)

func updateLocalAddressesCache(locallog zerolog.Logger) {
	// Get interface addresses.
	ifaces, err := net.Interfaces()
	if err != nil {
		locallog.Error().Err(err).Msg("Failed to get information about local interfaces!")
		return
	}

	ownRecordsMutex.Lock()
	for _, iface := range ifaces {
		ifaceLogger := locallog.With().Str("interface", iface.Name).Logger()
		if logger.SuperVerbosive {
			ifaceLogger.Debug().Msg("Starting checking interface for IP addresses changes...")
		}

		_, addressesCacheFound := ownRecords[iface.Name]

		// Just warn that we have no cached things.
		if !addressesCacheFound {
			if logger.SuperVerbosive {
				ifaceLogger.Warn().Msg("Addresses cache for interface is empty, will (re)create it.")
			}
			// ToDo: make slice length configurable?
			ownRecords[iface.Name] = make([]dns.RR, 0, 64)
		}

		// Get IP adresses for that interface.
		addrs, err1 := iface.Addrs()
		if err1 != nil {
			ifaceLogger.Error().Err(err1).Msg("Failed to get list of IP addresses while getting interface IPs")
			continue
		}

		for _, addrRaw := range addrs {
			addressLogger := ifaceLogger.With().Str("IP", addrRaw.String()).Logger()
			if logger.SuperVerbosive {
				addressLogger.Debug().Msg("Checking IP address for validity...")
			}

			// Parse IP address.
			ip, _, err2 := net.ParseCIDR(addrRaw.String())
			if err2 != nil {
				addressLogger.Error().Err(err2).Msg("Failed to parse IP address, skipping")
				continue
			}

			// Check if parsed address is withing allowed range.
			// ToDo: exceptions, configuration, etc.
			var validIP bool
			for _, privateNet := range vars.PrivateNets {
				if privateNet.Contains(ip) {
					validIP = true
					break
				}
			}

			// We should ignore addresses that hasn't passed previous
			// check(s).
			if !validIP {
				if logger.SuperVerbosive {
					addressLogger.Warn().Msg("IP address isn't withing private IP addresses spaces, ignoring it")
				}
				continue
			}

			if logger.SuperVerbosive {
				addressLogger.Debug().Msg("Address can be used")
			}

			var isv6 bool
			addr := ip.To4()
			if addr == nil {
				addr = ip.To16()
				if addr != nil {
					isv6 = true
				} else {
					addressLogger.Fatal().Msg("Failed to parse IP address! This error should not happen, please create issue at https://gitlab.com/pztrn/ihava/issues")
				}
			}

			// Check if this address already added.
			var alreadyCached bool
			for _, cachedAddress := range ownRecords[iface.Name] {
				// ToDo: proper way to do that.
				if strings.Contains(cachedAddress.String(), addr.String()) {
					alreadyCached = true
					break
				}
			}

			if alreadyCached {
				if logger.SuperVerbosive {
					addressLogger.Debug().Msg("Address already present in records list")
				}
				continue
			}

			if isv6 {
				ownAAAA := new(dns.AAAA)
				ownAAAA.AAAA = addr
				ownAAAA.Hdr.Name = configuration.Cfg.Station.Hostname + "." + configuration.Cfg.Station.Domain + "."
				ownAAAA.Hdr.Class = dns.ClassINET
				ownAAAA.Hdr.Rrtype = dns.TypeAAAA
				// RFC compatability.
				ownAAAA.Hdr.Ttl = 1
				ownRecords[iface.Name] = append(ownRecords[iface.Name], ownAAAA)
			} else {
				ownA := new(dns.A)
				ownA.A = addr
				ownA.Hdr.Name = configuration.Cfg.Station.Hostname + "." + configuration.Cfg.Station.Domain + "."
				ownA.Hdr.Class = dns.ClassINET
				ownA.Hdr.Rrtype = dns.TypeA
				// RFC compatability
				ownA.Hdr.Ttl = 1
				ownRecords[iface.Name] = append(ownRecords[iface.Name], ownA)
			}
		}
	}

	if logger.SuperVerbosive {
		locallog.Debug().Msgf("Composed list of own addresses and services: %+v", ownRecords)
		locallog.Debug().Msg("IP addresses and records for this machine was updated")
	}
	ownRecordsMutex.Unlock()
}
