# github.com/miekg/dns v1.1.26
github.com/miekg/dns
# github.com/rs/zerolog v1.17.2
github.com/rs/zerolog
github.com/rs/zerolog/internal/cbor
github.com/rs/zerolog/internal/json
# golang.org/x/crypto v0.0.0-20190923035154-9ee001bba392
golang.org/x/crypto/ed25519
golang.org/x/crypto/ed25519/internal/edwards25519
# golang.org/x/net v0.0.0-20190923162816-aa69164e4478
golang.org/x/net/bpf
golang.org/x/net/internal/iana
golang.org/x/net/internal/socket
golang.org/x/net/ipv4
golang.org/x/net/ipv6
# golang.org/x/sys v0.0.0-20190924154521-2837fb4f24fe
golang.org/x/sys/unix
golang.org/x/sys/windows
# gopkg.in/yaml.v2 v2.2.7
gopkg.in/yaml.v2
