package configuration

import (
	// stdlib
	"time"
)

type Timeouts struct {
	InterfaceAndAddressesCheck time.Duration `yaml:"interface_and_addresses_check"`
}

func (tm *Timeouts) CheckAndLoadDefaults() {
}
