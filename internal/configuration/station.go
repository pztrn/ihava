package configuration

import (
	// stdlib
	"os"
	"strings"
)

// Station represents local station configuration.
type Station struct {
	Hostname          string   `yaml:"hostname"`
	Domain            string   `yaml:"domain"`
	ForceFullHostname bool     `yaml:"force_full_hostname"`
	Timeouts          Timeouts `yaml:"timeouts"`
}

func (st *Station) CheckAndLoadDefaults() {
	if st.Hostname == "" {
		hostname, err := os.Hostname()
		if err != nil {
			hostname = "Unknown"
		}

		// As we're about mDNS and such we should take only first part of
		// hostname if not forced otherwise in configuration file.
		if !Cfg.Station.ForceFullHostname {
			hostnameSplitted := strings.Split(hostname, ".")
			// Work around possible things like ".host.na.me".
			for _, hostPart := range hostnameSplitted {
				if len(hostPart) != 0 {
					hostname = hostPart
					break
				}
			}
		}
		st.Hostname = hostname
	}
	log.Info().Str("hostname", st.Hostname).Msg("Got hostname")

	if st.Domain == "" {
		st.Domain = "local"
	}

	st.Timeouts.CheckAndLoadDefaults()
}
