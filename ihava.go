package main

import (
	// stdlib
	"flag"
	"os"
	"os/signal"
	"syscall"

	// local
	"go.dev.pztrn.name/ihava/domains/dnsrecords"
	"go.dev.pztrn.name/ihava/domains/listener"
	"go.dev.pztrn.name/ihava/internal/configuration"
	"go.dev.pztrn.name/ihava/internal/logger"
	"go.dev.pztrn.name/ihava/internal/vars"
	//"gitlab.com/pztrn/ihava/server"
)

func main() {
	logger.Initialize()

	logger.Logger.Info().Msg("Starting IHAVA DNS-SD/mDNS server...")

	vars.CompilePrivateNetsDefinitions()

	configuration.Initialize()
	//server.Initialize()
	dnsrecords.Initialize()
	listener.Initialize()

	flag.Parse()

	configuration.Load()
	dnsrecords.Start()
	listener.Start()
	//server.Start()

	// CTRL+C handler.
	signalHandler := make(chan os.Signal, 1)
	shutdownDone := make(chan bool, 1)
	signal.Notify(signalHandler, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-signalHandler
		logger.Logger.Info().Msg("Shutting down IHAVA...")
		listener.Shutdown()
		//server.Srv.Shutdown()
		shutdownDone <- true
	}()

	<-shutdownDone
	logger.Logger.Info().Msg("Ihava server shutted down")
	os.Exit(0)
}
