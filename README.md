# IHAVA

Ihava is an DNS-SD and mDNS server written in Go aimed to make generic systems and network administrators happy.

Generic use cases:

* Dual stack LAN and neccessity to use domain names (e.g. DHCPv6) or just for convenience.
* Neccessity to connect two or more subnets for mDNS resolving.

Not supported use cases:

* Kubernetes, Docker and all things around them. It **may** work but no guarantees provided. Patches welcome.

## The history

I was thinking about making my LAN to work with both IPv4 and IPv6 and encounter a problem with AVAHI - it just don't work as generic administrator might expect. For example, it can get data races when hostname is using IPv4 and IPv6 simultaneously, or if you'll specify a hostname in configuration file - reflector between subnets stops working.

Also there is no easy way to connect two or more subnets without tinkering with VLANs, which is also bad.

Also "one more DNS server" is a pretty bad approach, so ihava will (eventually) also be a caching DNS server for general use.

## Installation

It should be enough to say:

```
go get -u go.dev.pztrn.name/ihava
```

and create approriate systemd unit (*TBW*).

## Configuration

Example configuration file with comments about every option is available [here](/ihava.dist.yaml).

### Logging level

By default Ihava will log on INFO level. To get debug output you should specify ``IHAVA_LOGGER_LEVEL`` environment variable and set it to ``DEBUG``. Other possible levels:

* ``INFO`` (default)
* ``WARN``
* ``ERROR``
* ``FATAL``

### Advanced floodish logging (for debugging)

## Developing

All development activity happens [on my Gitea](https://sources.dev.pztrn.name/pztrn/ihava). Please fill bug reports and feature requests there.

Specify ``IHAVA_LOGGER_SUPERVERBOSIVE`` environment variable to get super-verbosive output. Valid values are ``true`` and ``false``.

## FAQ

### Do I have to install it on client machines with Avahi up and running?

Not neccessarily. Ihava can replace Avahi and also can work with it. But not on one machine. It's fine if you have Ihava installed on RPi and Avahi on local machine.