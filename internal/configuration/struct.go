package configuration

// Config represents whole configuration file structure.
type Config struct {
	Station Station `yaml:"station"`
}

// CheckAndLoadDefaults check configuration file for values and load
// default ones if needed, so other parts of Ihava will not do
// strange things.
func (cfg *Config) CheckAndLoadDefaults() {
	cfg.Station.CheckAndLoadDefaults()
}
