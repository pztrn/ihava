package listener

import (
	// stdlib
	"net"
	"sync"
	"time"

	// local
	"go.dev.pztrn.name/ihava/internal/logger"

	// other
	"github.com/rs/zerolog"
)

var (
	log zerolog.Logger

	shouldShutdown      bool
	shouldShutdownMutex sync.Mutex

	listeners      []*InterfaceListener
	listenersMutex sync.Mutex
)

// Initialize initializes package.
func Initialize() {
	log = logger.Logger.With().Str("package", "listener").Logger()

	log.Info().Msg("Initializing subsystem...")
}

// Shutdown sets shutdown flag and wait for interface listeners to stop.
func Shutdown() {
	shouldShutdownMutex.Lock()
	shouldShutdown = true
	shouldShutdownMutex.Unlock()

	// ToDo: check if all interface listeners are stopped.
	listenersMutex.Lock()
	for _, listener := range listeners {
		listener.Shutdown()
	}
	listenersMutex.Unlock()

	for {
		listenersStopped := true

		listenersMutex.Lock()
		for _, listener := range listeners {
			if !listener.IsStopped() {
				listenersStopped = false
			}
		}
		listenersMutex.Unlock()

		if !listenersStopped {
			time.Sleep(time.Second * 1)
			continue
		}

		break
	}
}

// Start starts neccessary listeners.
func Start() {
	log.Info().Msg("Starting listeners...")

	ifaces, err := net.Interfaces()
	if err != nil {
		log.Error().Err(err).Msg("Failed to get information about local interfaces!")
		return
	}

	// ToDo: interfaces can be added and removed. Make it check them on timeout!
	listenersMutex.Lock()
	for _, iface := range ifaces {
		il := &InterfaceListener{}
		il.Initialize(iface.Name)
		listeners = append(listeners, il)
	}
	log.Info().Int("listeners count", len(listeners)).Msg("Listeners started")
	listenersMutex.Unlock()
}
