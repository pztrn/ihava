module go.dev.pztrn.name/ihava

go 1.13

require (
	github.com/miekg/dns v1.1.26
	github.com/rs/zerolog v1.17.2
	gitlab.com/pztrn/ihava v0.0.0-20190628102846-f893378484f8 // indirect
	gopkg.in/yaml.v2 v2.2.7
)
