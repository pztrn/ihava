package vars

const (
	IPv4mdns = "224.0.0.251"
	IPv6mdns = "ff02::fb"
	MDNSPort = 5353
)
