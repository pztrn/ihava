package listener

import (
	// stdlib
	"net"
	"strings"
	"sync"
	"time"

	// local
	"go.dev.pztrn.name/ihava/domains/dnsrecords"
	"go.dev.pztrn.name/ihava/internal/configuration"
	"go.dev.pztrn.name/ihava/internal/logger"
	"go.dev.pztrn.name/ihava/internal/vars"

	// other
	"github.com/miekg/dns"
	"github.com/rs/zerolog"
)

// InterfaceListener represents controlling structure for UDP listeners.
// This structure represents single interface with listeners, for every
// valid interface own instance of InterfaceListener will be created.
type InterfaceListener struct {
	log zerolog.Logger

	interfaceName string

	// Listener shutted down flag.
	shuttedDown      bool
	shuttedDownMutex sync.Mutex

	// Active UDP servers.
	v4listener *net.UDPConn
	v6listener *net.UDPConn
}

// Returns our interface data.
func (l *InterfaceListener) getOurInterface() *net.Interface {
	ifaces, err := net.Interfaces()
	if err != nil {
		l.log.Error().Err(err).Msg("Failed to get information about local interfaces!")
		return nil
	}

	var ourInterface *net.Interface
	for _, iface := range ifaces {
		if iface.Name == l.interfaceName {
			ourInterface = &iface
			break
		}
	}

	return ourInterface
}

// Returns list of IP addresses for interface this listener is working
// on.
func (il *InterfaceListener) getIPs() []net.IP {
	var ips []net.IP

	ourIface := il.getOurInterface()
	if ourIface == nil {
		il.log.Error().Msg("Failed to find interface on local machine!")
		return ips
	}

	if ourIface == nil {
		il.log.Error().Msg("Failed to get interface data!")
		return ips
	}

	addrs, err1 := ourIface.Addrs()
	if err1 != nil {
		il.log.Error().Err(err1).Msg("Failed to get list of IP addresses while getting interface IPs")
		return ips
	}
	for _, addr := range addrs {
		parsedIP, _, err := net.ParseCIDR(addr.String())
		if err != nil {
			il.log.Error().Err(err).Msg("Failed to parse IP address and subnet while getting interface IPs")
			continue
		}

		// We should only return IPs that in ranges defined by RFC 1918.
		for _, privateNet := range vars.PrivateNets {
			if privateNet.Contains(parsedIP) {
				if logger.SuperVerbosive {
					il.log.Debug().IPAddr("IP", parsedIP).Msg("Found IP on interface")
				}
				ips = append(ips, parsedIP)
			}
		}
	}

	il.log.Debug().Int("private ips count", len(ips))

	return ips
}

func (il *InterfaceListener) Initialize(interfaceName string) {
	il.interfaceName = interfaceName
	il.log = log.With().Str("interface", interfaceName).Logger()

	il.log.Info().Msg("Starting UDP listeners...")

	il.startOrShutdownListeners()
	go il.startOrShutdownListenersWorker()

}

// IsStopped returns boolean value that represents listener's status.
// If value is false - listener is still working, otherwise it'll be
// true.
func (il *InterfaceListener) IsStopped() bool {
	il.shuttedDownMutex.Lock()
	defer il.shuttedDownMutex.Unlock()
	return il.shuttedDown
}

// Function that works with received DNS requests in separate goroutine.
func (il *InterfaceListener) receiver(conn *net.UDPConn) {
	locallog := il.log.With().Str("listener", conn.LocalAddr().String()).Logger()
	locallog.Info().Str("IP address", conn.LocalAddr().String()).Msg("Ready to receive data")

	// ToDo: read RFC about that.
	buf := make([]byte, 65536)

	for {
		// Read data from listener.
		readBytes, requesterIPRaw, err := conn.ReadFrom(buf)
		if err != nil {
			// If we're about to use closed network socket - then this
			// isn't an error actually, we closed it due to Shutdown()
			// call.
			if !strings.Contains(err.Error(), "use of closed network connection") {
				locallog.Error().Err(err).Msg("Error occured during read data from socket")
			}
			// But we should break on any occured read error.
			locallog.Warn().Msg("Reading from socket due to error or shutdown procedure")
			break
		}
		locallog.Debug().Int("length", readBytes).Msg("Got data on socket")

		// Check if request came from allowed (e.g. private network) address.
		// Due to RFC requests that came from not valid addresses
		// (like public ones) should be silently ignored.
		host, _, err1 := net.SplitHostPort(requesterIPRaw.String())
		if err1 != nil {
			locallog.Error().Err(err1).Msg("Failed to parse requester's IP address")
			continue
		}
		requesterIP := net.ParseIP(host)
		if requesterIP == nil {
			locallog.Error().Err(err1).Msg("Failed to parse requester's IP address")
			continue
		}

		// Before parsing request we should be sure that request came
		// from interface we're listening on, to avoid packet storms.
		// For that to work we will get interface's subnet and check
		// if requester's IP address are within subnet attached on
		// interface.
		ourIface := il.getOurInterface()
		if ourIface == nil {
			locallog.Error().Msg("Failed to find interface on local machine!")
			continue
		}

		localAddresses, err2 := ourIface.Addrs()
		if err2 != nil {
			locallog.Error().Err(err2).Msg("Failed to get IP addresses for interface")
			continue
		}

		var allowedToParse bool
		for _, addr := range localAddresses {
			_, ifaceNetwork, err3 := net.ParseCIDR(addr.String())
			if err3 != nil {
				locallog.Error().Err(err3).Msg("Failed to parse local address")
			}

			if ifaceNetwork.Contains(requesterIP) {
				locallog.Debug().IPPrefix("checked prefix", (*ifaceNetwork)).IPAddr("checked address", requesterIP).Msg("Message received from allowed subnet")
				allowedToParse = true
			}
		}

		if !allowedToParse {
			locallog.Warn().Msg("Request came on different interface, skipping here")
			continue
		}

		// Logger for this request.
		reqlog := locallog.With().IPAddr("requester's IP", requesterIP).Logger()

		// When all checks completed - put a message in logs.
		reqlog.Debug().Int("length", readBytes).Msg("Received mDNS multicast request")

		// Parse request.
		var msg dns.Msg
		err4 := msg.Unpack(buf[:readBytes])
		if err4 != nil {
			reqlog.Error().Err(err4).Msg("Failed to parse DNS packet")
		}

		// Validate request.
		// Check RFC things.
		// First we should check if opcode for query is zero. Otherwise we
		// should ignore it silently.
		if msg.Opcode != dns.OpcodeQuery {
			reqlog.Error().Msg("OPCODE in request isn't zero, ignoring request")
			continue
		}
		// Second we should check that response code is zero, otherwise we
		// should ignore request silently.
		if msg.Rcode != 0 {
			reqlog.Error().Msg("Response code in request isn't zero, ignoring request")
			continue
		}
		// Also we should check if truncated bit was set. If so - do nothing
		// as we isn't support TC bits ATM.
		// ToDo: implement truncated bit support.
		if msg.Truncated {
			reqlog.Error().Msg("Truncated request isn't supported now, ignoring request")
			continue
		}

		// At the end we should be sure that at least one of requests
		// came about our own domain. All other requests should be
		// ignored.
		var validQuestions []dns.Question
		for _, question := range msg.Question {
			if question.Name != configuration.Cfg.Station.Hostname+"."+configuration.Cfg.Station.Domain+"." {
				reqlog.Warn().Str("requested domain", msg.Question[0].Name).Msg("Received request about non-our domain name, ignoring...")
				continue
			} else {
				validQuestions = append(validQuestions, question)
			}
		}
		if len(validQuestions) == 0 {
			reqlog.Debug().Msg("No valid DNS questions for us, ignoring request")
			continue
		}

		reqlog.Debug().Msgf("Received valid mDNS request: %+v", msg)

		// If everything is okay - we should get DNS records for interface
		// we're received request on and respond.
		response := dnsrecords.GetRecordsForInterfaceName(ourIface.Name, msg.Id)
		reqlog.Debug().Msgf("Got response for interface: %+v", response)
		// Client might request multicast or unicast answer, respond
		// accordingly.
		data, err := response.Pack()
		if err != nil {
			reqlog.Error().Err(err).Msg("Failed to pack response!")
			continue
		}

		responseTo := requesterIPRaw.(*net.UDPAddr)
		var responseError error
		if responseTo.IP.To4() != nil {
			_, responseError = il.v4listener.WriteToUDP(data, responseTo)
		} else {
			_, responseError = il.v6listener.WriteToUDP(data, responseTo)
		}

		if responseError != nil {
			reqlog.Error().Err(responseError).Msg("Failed to send response back to requester")
		}
	}
}

// Shutdown stops IPv4/IPv6 listeners.
func (il *InterfaceListener) Shutdown() {
	il.log.Debug().Msg("Stopping listeners...")
	if il.v4listener != nil {
		err := il.v4listener.Close()
		if err != nil {
			il.log.Warn().Err(err).Msg("Failed to stop IPv4 listener")
		}
	}

	if il.v6listener != nil {
		err := il.v6listener.Close()
		if err != nil {
			il.log.Warn().Err(err).Msg("Failed to stop IPv6 listener")
		}
	}

	il.shuttedDownMutex.Lock()
	il.shuttedDown = true
	il.shuttedDownMutex.Unlock()
}

// Starts or shutdowns listeners. For every new acceptable IP address
// new listener will be started, and if IP address disappears - listener
// will be stopped. Should be launched in separate goroutine.
func (il *InterfaceListener) startOrShutdownListeners() {
	// Before really starting UDP listeners we should check that addresses
	// on our interface are from RFC1918 ranges.
	// ToDo: possibility to add exceptions.
	interfaceIPs := il.getIPs()

	// Our interface data.
	ourIface := il.getOurInterface()

	// Check current IPs and, if possible, start listeners on them.
	var shouldListenOnV4 bool
	var shouldListenOnV6 bool
	for _, ip := range interfaceIPs {
		// Check what IP address we have here.
		ip4 := ip.To4()
		if ip4 != nil {
			shouldListenOnV4 = true
		} else {
			ip6 := ip.To16()
			if ip6 != nil {
				shouldListenOnV6 = true
			}
		}
	}

	if logger.SuperVerbosive {
		il.log.Debug().Bool("should listen on IPv4", shouldListenOnV4).Bool("should listen on IPv6", shouldListenOnV6).Msg("Will start these listeners")
	}

	var ipv4WasStarted bool
	var ipv6WasStarted bool
	var ipv4WasStopped bool
	var ipv6WasStopped bool

	if shouldListenOnV4 && il.v4listener == nil {
		il.log.Debug().Msg("Starting IPv4 listener...")
		var err error
		il.v4listener, err = net.ListenMulticastUDP("udp4", ourIface, &net.UDPAddr{IP: net.ParseIP(vars.IPv4mdns), Port: vars.MDNSPort})
		if err != nil {
			il.log.Error().Err(err).Msg("Failed to start IPv4 multicast listener")
		}
		go il.receiver(il.v4listener)
		ipv4WasStarted = true

	}

	if shouldListenOnV6 && il.v6listener == nil {
		il.log.Debug().Msg("Starting IPv6 listener...")
		var err error
		il.v6listener, err = net.ListenMulticastUDP("udp6", ourIface, &net.UDPAddr{IP: net.ParseIP(vars.IPv6mdns), Port: vars.MDNSPort})
		if err != nil {
			il.log.Error().Err(err).Msg("Failed to start IPv6 multicast listener")
		}
		go il.receiver(il.v6listener)
		ipv6WasStarted = true
	}

	// ToDo: check it. Should work.
	if !shouldListenOnV4 && il.v4listener != nil {
		il.log.Warn().Msg("Stopping IPv4 listener as no IPv4 addresses present on interface.")
		il.v4listener.Close()
		il.v4listener = nil
		ipv4WasStopped = true
	}

	// ToDo: check it. Should work.
	if !shouldListenOnV6 && il.v6listener != nil {
		il.log.Warn().Msg("Stopping IPv6 listener as no IPv6 addresses present on interface")
		il.v6listener.Close()
		il.v6listener = nil
		ipv6WasStopped = true
	}

	if ipv4WasStarted {
		il.log.Debug().Msg("IPv4 listener was (re)started on interface")
	}

	if ipv6WasStarted {
		il.log.Debug().Msg("IPv6 listener was (re)started on interface")
	}

	if ipv4WasStopped {
		il.log.Debug().Msg("IPv4 listener was stopped on interface")
	}

	if ipv6WasStopped {
		il.log.Debug().Msg("IPv6 listener was stopped on interface")
	}

	if il.v4listener == nil && logger.SuperVerbosive {
		il.log.Warn().Msg("IPv4 listener wasn't started")
	}

	if il.v6listener == nil && logger.SuperVerbosive {
		il.log.Warn().Msg("IPv6 listener wasn't started")
	}

}

// Executes startOrShutdownListeners in timed manner. Should be
// launched in separate goroutine.
func (il *InterfaceListener) startOrShutdownListenersWorker() {
	ticker := time.NewTicker(configuration.Cfg.Station.Timeouts.InterfaceAndAddressesCheck)
	il.log.Debug().Msg("Starting listener watcher...")

	for range ticker.C {
		// Shutdown check.
		// ToDo: as this check executes every addresses checking timeout
		// IHAVA shutdown will be pretty slow. This should be reworked,
		// MRs welcome.
		shouldShutdownMutex.Lock()
		weShouldStop := shouldShutdown
		shouldShutdownMutex.Unlock()

		if weShouldStop {
			break
		}

		il.startOrShutdownListeners()
	}

	il.log.Info().Msg("Listener watcher stopped")
}
